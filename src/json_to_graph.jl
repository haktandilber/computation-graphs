using JSON, Graphs, TikzGraphs, TikzPictures, LaTeXStrings
using Graphs, GraphPlot, Compose, Cairo
using SparseArrays: SparseMatrixCSC, sparse
using Base: OneTo
using LinearAlgebra: eigen
using ArnoldiMethod: SR
using Genie, Genie.Router
using Genie.Renderer, Genie.Renderer.Html, Genie.Renderer.Json
using DataStructures

struct CompGraph
    graphName::String
    names::Vector{String}
    type::Vector{String}
    operation::Vector{String}
    annotation::Vector{String}
    precedence
    argument
    g
end

CompGraph() = CompGraph("", [], [], [], [], [], [], [])


function graph_to_json(cgraph)
    n = 0
    S = "{\"graph\":["
    for i in range(1, length(cgraph.names))
        strToAdd = "{" * "\"name\":\"" * cgraph.names[i] * "\",\"anno\":\"" * cgraph.annotation[i] * "\",\"op\":\"" * cgraph.operation[i] * "\",\"type\":\"" * cgraph.type[i] * "\"},"
        S = S * strToAdd
    end
    S = S * "{\"name\":\"last\",\"anno\":\"last\",\"op\":\"last\",\"type\":\"last\"}]}"
    return S
end

function loaded_graphs(dir)

    dir = readdir(dir)
    S = "{\"graphs\":["
    for elem in dir
        if !isdir(elem)
            strToAdd = "{" * "\"name\":\"" * elem * "\"},"
            S = S * strToAdd
        end
    end
    S = S * "{\"name\":\"last\"}]}"
    return S
end


function graph_to_str(cgraph)
    n = 0
    S = "["
    for name in cgraph.names
        S = S * "\"" * "" * name * "\","
        n = n + 1
    end
    S = S * "\"last\"]"
    return S
end


function graph_to_op(cgraph)
    n = 0
    S = "["
    for oper in cgraph.operation
        S = S * "\"" * ": " * oper * "\","
        n = n + 1
    end
    S = S * "\"last\"]"
    return S
end


function graph_to_edges(cgraph)
    n = 0
    S = "["
    for edge in edges(cgraph.g)
        if haskey(cgraph.precedence, string(src(edge)) * "-" * string(dst(edge)))
            S = S * "\"" * string(src(edge)) * "\",\"" * string(dst(edge)) * "\",\"" * string(cgraph.precedence[string(src(edge))*"-"*string(dst(edge))]) * "\","
        else
            S = S * "\"" * string(src(edge)) * "\",\"" * string(dst(edge)) * "\",\"-1\","
        end
    end
    S = S * "\"0\"]"
    return S
end


function generate_name(node)
    if node["OPNAME"] == "null"
        return node["NAME"]
    else
        return node["NAME"]
    end
end

function draw_graph(filename, graph::CompGraph)
    draw(PNG("./" * filename, 16cm, 16cm), gplot(graph.g, nodelabel=graph.names, nodesize=5.0, nodelabelsize=0.5))
end

function update_graph(graphmodel, graph::CompGraph)
    id_to_graphid = Vector{Integer}()
    names = Vector{String}()
    operations = Vector{String}()
    types = Vector{String}()
    annotations = Vector{String}()
    precedence = OrderedDict{String,String}()
    argument = OrderedDict{String,String}()

    nodes = graphmodel["root"]["mxCell"]
    #print(nodes)
    numberofnodes = 0
    for node in nodes
        if haskey(node, "_target") == true
            #print(node["_source"] * " to " * node["_target"] * "\n")
        elseif (haskey(node, "_value") == true) && node["_parent"] == "1"
            #print(node["_value"] * "\n")
            push!(id_to_graphid, parse(Int, node["_id"]))
            push!(names, node["_value"])
            if contains(node["_style"], "_type=NORMAL")
                push!(types, "NORMAL")
            elseif contains(node["_style"], "_type=INPUT")
                push!(types, "INPUT")
            elseif contains(node["_style"], "_type=OUTPUT")
                push!(types, "OUTPUT")
                print(node["_style"] * " doesnt include normal or input\n")
            elseif contains(node["_style"], "_type=COMPOUND")
                push!(types, "COMPOUND")
                print(node["_style"] * " is compound node\n")
            else
                push!(types, "COMPOUND")
                print(node["_style"] * " is compound node\n")
            end
            numberofnodes = numberofnodes + 1
        end
        #print("\n")
    end
    g = SimpleDiGraph(numberofnodes)
    for node in nodes
        if haskey(node, "_target") == true
            add_edge!(g, findfirst(isequal(parse(Int, node["_source"])), id_to_graphid), findfirst(isequal(parse(Int, node["_target"])), id_to_graphid))
        end
        if haskey(node, "_value") == true && haskey(node, "_source") == true
            if node["_value"] != ""
                print("has value " * node["_value"] * "\n")
                #precedence[parse(Int,node["_source"])][parse(Int,node["_target"])] = parse(Int,node["_value"])
                sourceid = findfirst(isequal(parse(Int, node["_source"])), id_to_graphid)
                targetid = findfirst(isequal(parse(Int, node["_target"])), id_to_graphid)
                precedence[string(sourceid)*"-"*string(targetid)] = node["_value"]
            end
        end
        if (haskey(node, "_value") == true) && (haskey(node, "_style") == true) && findfirst("COMPOUND", node["_style"]) !== nothing # || (node["_parent"]=="1" && haskey(node, "_target")==false))
            print("here: " * string(findfirst(isequal("COMPOUND"), node["_style"])))
            s = findlast(isequal('='), node["_style"])
            e = length(node["_style"])
            if s !== Nothing && e !== Nothing
                print(node["_style"][s:e])
                print(node["_style"])
                push!(operations, node["_style"][s+1:e-1])
            else
                print("nothing")
            end
        elseif (haskey(node, "_value") == true) && node["_parent"] != "1" && node["_style"] == "annotext;"
            push!(annotations, node["_value"])
        elseif (haskey(node, "_value") == true) && node["_parent"] != "1"
            push!(operations, node["_value"])
        elseif (haskey(node, "selected") == true)
            push!(operations, node["selected"])
            print("has selected")
        elseif (haskey(node, "_selected") == true)
            push!(operations, node["_selected"])
            print("has _selected")
        end

    end
    #draw_graph("update_test.png", CompGraph("graph", names, types, operations, annotations, precedence, g))
    return CompGraph("graph", names, types, operations, annotations, precedence, argument, g)
end


function generate_graph(filename)
    f = open(filename, "r")
    S = read(f, String)
    close(f)
    j = JSON.parse(S)
    g = SimpleDiGraph(parse(Int, j["metadata"]["NODES"]))

    global count = 1
    names = Vector{String}()
    operations = Vector{String}()
    types = Vector{String}()
    annotations = Vector{String}()
    precedence = OrderedDict{String,String}()
    argument = OrderedDict{String,String}()

    arredgeFrom = Vector{Int}()
    arredgeTo = Vector{String}()
    targetNumber = Vector{String}()
    nodes = j["nodes"]

    for node in nodes
        if generate_name(node) in names
            global edgeFrom = findfirst(isequal(generate_name(node)), names)
        else
            push!(names, generate_name(node))
            push!(operations, node["OPNAME"])
            push!(types, node["NODE_TYPE"])
            push!(annotations, node["ANNOTATION"])
            global edgeFrom = count
            global count = count + 1
        end

        for target in node["TARGETS"]
            push!(arredgeFrom, edgeFrom)
            push!(arredgeTo, target["TARGET"])
            if target["NUMBER"] != "-1"
                #precedence[string(edgeFrom)*"-"*string(edgeTo)] = parse(Int, target["NUMBER"])
                push!(targetNumber, target["NUMBER"])
            else
                push!(targetNumber, "-1")
            end

        end
    end

    for i in range(1, length(arredgeFrom))
        edgeTo = findfirst(isequal(arredgeTo[i]), names)
        add_edge!(g, arredgeFrom[i], edgeTo)
        if targetNumber[i] != "-1"
            precedence[string(arredgeFrom[i])*"-"*string(edgeTo)] = targetNumber[i]
        end
    end
    return CompGraph("graph", names, types, operations, annotations, precedence, argument, g)
end


function add_to_graph(filename, graph::CompGraph)
    f = open(filename, "r")
    S = read(f, String)
    close(f)
    j = JSON.parse(S)
    #print(j)
    g = SimpleDiGraph(parse(Int, j["metadata"]["NODES"]) + length(graph.names) + 2)

    for edge in edges(graph.g)
        add_edge!(g, src(edge), dst(edge))
    end

    global count = length(graph.names) + 1
    names = graph.names
    operations = graph.operation
    types = graph.type
    annotations = graph.annotation
    precedence = graph.precedence
    argument = graph.argument
    nodes = j["nodes"]

    for node in nodes
        if generate_name(node) in names
            global edgeFrom = findfirst(isequal(generate_name(node)), names)
        else
            push!(names, generate_name(node))
            push!(operations, node["OPNAME"])
            push!(types, node["NODE_TYPE"])
            push!(annotations, node["ANNOTATION"])
            global edgeFrom = count
            global count = count + 1
        end

        for target in node["TARGETS"]
            foundNode = 0
            for node2 in nodes
                if node2["NAME"] == target["TARGET"]
                    foundNode = node2
                end
            end
            if foundNode != 0 && (generate_name(foundNode)) in names
                #print("FOUND\n")
                global edgeTo = findfirst(isequal(generate_name(foundNode)), names)
                if target["NUMBER"] != "-1"

                    precedence[string(edgeFrom)*"-"*string(edgeTo)] = target["NUMBER"]
                end
            else
                global edgeTo = count
                push!(names, generate_name(foundNode))
                push!(operations, foundNode["OPNAME"])
                push!(types, foundNode["NODE_TYPE"])
                push!(annotations, foundNode["ANNOTATION"])
                if target["NUMBER"] != "-1"

                    precedence[string(edgeFrom)*"-"*string(edgeTo)] = target["NUMBER"]
                end
                global count = count + 1
            end
            add_edge!(g, edgeFrom, edgeTo)


        end



    end

    return CompGraph("graph", names, types, operations, annotations, precedence, argument, g)
end


function graph_to_code(graph::CompGraph, name = "graph_function")
    visited = []
    str = "def "*name*"("
    retstr = ""
    lines = Stack{String}()
    consts = Stack{String}()
    toCompile = Stack{String}()
    #print("def graph_function(")
    i = 0
    for i in range(1, length(graph.names))
        if graph.type[i] == "INPUT"
            if length(graph.names[i]) > 4
                if graph.names[i][1:5] == "const"
                    if graph.annotation[i] != "ann."
                        push!(consts, " " * graph.names[i] * " = " * graph.operation[i] * " #" * graph.annotation[i] * " \n")
                    else
                        push!(consts, " " * graph.names[i] * " = " * graph.operation[i] * "\n")
                    end
                    continue
                end
            end
            if graph.operation[i] != "null" && graph.operation[i] != ""
                #print(graph.names[i] * " = " * graph.operation[i] * ",")
                str = str * graph.names[i] * " = " * graph.operation[i] * ","
            else
                #print(graph.names[i] * ",")
                str = str * graph.names[i] * ","
            end

        end
        i = i + 1
    end
    str = str[1:length(str)-1]
    #print("):")
    str = str * "):\n"

    while isempty(consts) == false
        line = pop!(consts)
        #print(line)
        str = str * line
    end
    outputs = Vector{String}()
    outputnumbers = Vector{Integer}()
    i = 0
    for i in range(1, length(graph.names))
        if graph.type[i] == "OUTPUT"
            push!(outputs, graph.names[i])
            push!(outputnumbers, i)
        end
    end

    if length(outputs) == 1
        #print(" return ")
        retstr = retstr * " return " * outputs[1]
        #print(outputs[1])
    else
        #print(" return (")
        retstr = retstr * " return ("
        for output in outputs
            #print(output)
            #print(",")
            retstr = retstr * output * ","
        end
        retstr = retstr[1:length(retstr)-1]
        retstr = retstr * ")"
    end
    #print("\n")
    str = str * "\n"
    for i in range(1, length(outputs))
        recursive_traverse(graph, outputnumbers[i], lines, visited, toCompile)
        while isempty(lines) == false
            line = pop!(lines)
            #print(line)
            str = str * line
        end
    end
    str = str * "\n" * retstr

    while isempty(toCompile) == false
        item = pop!(toCompile)
        print(item)
        path= "../loaded/"*item
        print(path)
        str = str *"\n\n"* graph_to_code(generate_graph(path),item[1:length(item)-5])
    end
    return str
end

function operation(op)
    if (op == "ADD") || (op == "add")
        return "+"
    end
    if (op == "SUBS") || (op == "subs")
        return "-"
    end
    if op == "MULT" || op == "mult"
        return "*"
    end
    if op == "DIV" || op == "div"
        return "/"
    end
    if op == "REM" || op == "rem"
        return "%"
    end
    if op == "POW" || op == "pow"
        return "**"
    end
    return op


end

function recursive_traverse(graph::CompGraph, node, lines, visited, toCompile)
    if length(inneighbors(graph.g, node)) == 0
        return
    end
    pred = 0
    line = graph.names[node] * " = "
    append!(visited, node)
    if graph.type[node] == "COMPOUND"
        push!(toCompile,graph.operation[node])
        line = line * graph.operation[node][1:length(graph.operation[node])-5]*"("
        for neighbor in inneighbors(graph.g, node)
            if haskey(graph.precedence, string(neighbor) * "-" * string(node))
                line = line * graph.precedence[string(neighbor)*"-"*string(node)] * " = " *graph.names[neighbor] * ", "
            end
        end
        line = line[1:length(line)-2] * " )   "
    else
        for neighbor in inneighbors(graph.g, node)
            if haskey(graph.precedence, string(neighbor) * "-" * string(node))
                #print("here is a precedence POG\n")
                pred = 1
                break
            end
            line = line * graph.names[neighbor] * " "
            line = line * operation(graph.operation[node]) * " "
        end
        if pred == 1
            nodes = [0, 0]
            values = [0, 0]
            i = 1
            for neighbor in inneighbors(graph.g, node)
                print("valuesi: "*graph.precedence[string(neighbor)*"-"*string(node)]*"\n")
                nodes[i] = neighbor
                values[i] = parse(Int,graph.precedence[string(neighbor)*"-"*string(node)])
                i = 2
            end
            if values[1] > values[2]
                line = line * graph.names[nodes[2]] * " " * operation(graph.operation[node]) * " " * graph.names[nodes[1]] * "    "
            else
                line = line * graph.names[nodes[1]] * " " * operation(graph.operation[node]) * " " * graph.names[nodes[2]] * "    "
            end
            pred = 0
        end
    end
    if graph.annotation[node] == "ann."
        line = " " * line[1:length(line)-3]
    else
        line = " " * line[1:length(line)-3] * " #" * graph.annotation[node]
    end
    push!(lines, line * "\n")
    for neighbor in inneighbors(graph.g, node)
        if findfirst(isequal(neighbor), visited) === nothing
            recursive_traverse(graph, neighbor, lines, visited,toCompile)
        end
    end
end


function graph_to_file(graph::CompGraph)
    array = []
    graphDict = Dict()
    graphDict["metadata"] = Dict()
    graphDict["metadata"]["NAME"] = graph.graphName
    graphDict["metadata"]["NODES"] = string(length(graph.names))
    graphDict["nodes"] = []
    for i in range(1, length(graph.names))
        new = Dict()
        new["NODE_TYPE"] = graph.type[i]
        new["OPNAME"] = graph.operation[i]
        new["NAME"] = graph.names[i]
        new["ANNOTATION"] = graph.annotation[i]
        new["INPUTS"] = []
        new["TARGETS"] = []
        push!(array, new)
    end
    for edge in edges(graph.g)
        targetDict = Dict()
        targetDict["TARGET"] = graph.names[dst(edge)]
        if haskey(graph.precedence, string(src(edge)) * "-" * string(dst(edge)))
            targetDict["NUMBER"] = string(graph.precedence[string(src(edge))*"-"*string(dst(edge))])
        else
            targetDict["NUMBER"] = "-1"
        end
        push!(array[src(edge)]["TARGETS"], targetDict)
        push!(array[dst(edge)]["INPUTS"], graph.names[src(edge)])
    end
    for elem in array
        push!(graphDict["nodes"], elem)
    end
    return graphDict
end