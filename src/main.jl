include("json_to_graph.jl")
using  JSON, Graphs, TikzGraphs, TikzPictures, LaTeXStrings
using Graphs,GraphPlot,Compose,Cairo
using SparseArrays: SparseMatrixCSC, sparse
using Base: OneTo
using LinearAlgebra: eigen
using ArnoldiMethod: SR
using Genie, Genie.Router
using Genie.Renderer, Genie.Renderer.Html, Genie.Renderer.Json, Genie.Requests


global cGraph = CompGraph()


f = open("editor.html", "r")
    S=read(f,String)
    close(f)

f = open("htmllabel.html", "r")
    S2=read(f,String)
    close(f)


form = """
<body style="background-color:lightgray;">
<style>
.center {
  margin: auto;
  width: 60%;
  border: 3px solid #73AD21;
  padding: 10px;
}
</style>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<div class="card" class="center" style="width: 30rem;text-align: center;margin: auto; padding-top: 50px;">
  <div class="card-body">
<h1>Computational Graph Editor</h1> <br>
<a href="./lib">Graph library</a>
<p>Please upload your json graph file or create new graph. </p><br>
<form action="/" method="POST" enctype="multipart/form-data">
  <input type="file"  class="form-control-file" name="yourfile" /><br/>
  <input type="submit" class="btn btn-primary" value="Upload graph" />
</form>
or
<br/>
<form action="/" method="POST" enctype="multipart/form-data">

<br/>
  <input type="submit" class="btn btn-primary" value="New graph" />
</form>
<p><b>Editor usage:</b> <br>
<span style="color:red">Red</span> nodes are the <span style="padding: 2px; border: 1px solid #b85450;background:#f8cecc;">input nodes</span>. <br>
<span style="color:blue">Blue</span> nodes are the <span style="padding: 2px; border: 1px solid #b85450;background:#87CEEB;"> intermediary normal nodes. </span> <br>
<span style="color:green">Green</span> nodes are the <span style="padding: 2px; border: 1px solid #b85450;background:#04ff00;">output nodes.</span> <br>
<span style="color:olive">Yellow</span> nodes are the <span style="padding: 2px; border: 1px solid #b85450;background:#FFFF33;">compound nodes.</span> These represent the loaded graphs. <br>
Use upper buttons to change type of the selected node. <br>
Create edges by dragging from source node to target node. <br>
Double click node properties to edit the property. <br>
Drag node from left toolbar to add a new node. <br>
</p>
<h6>Made by Haktan Bilgehan Dilber for GTU Graduation Project Fall22</h6>
</div>
</div>
"""

global lib = """
<body style="background-color:lightgray;">
<style>
.center {
  margin: auto;
  width: 60%;
  border: 3px solid #73AD21;
  padding: 10px;
}
</style>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<div class="card" class="center" style="width: 30rem;text-align: center;margin: auto; padding-top: 50px;">
  <div class="card-body">
<h1>Graph library</h1> <br>
<p>This page contains the loaded graphs in the graph library and new graphs can be loaded from this page. If your graph has compound nodes make sure that used corresponding graphs are also loaded.</p><br>
<form action="/lib" method="POST" enctype="multipart/form-data">
  <input type="file"  class="form-control-file" name="yourfile" /><br/>
  <input type="submit" class="btn btn-primary" value="Add to library" />
</form>
<br/>
<form action="/lib" method="POST" enctype="multipart/form-data">
</form>
<h1>Loaded Graphs</h1> <br>

"""
dir = readdir("../images")
println("\n","Files: \n", [elm for elm in dir if !isdir(elm)])
lock = ReentrantLock()

global isFileUploaded = 0
route("/") do
  html(form)
end

route("/lib") do
  dir = readdir("../loaded")
  toadd=""
  for elem in dir
    if !isdir(elem)
        toadd=toadd*elem*"<br>"
    end
  end
  toadd=toadd*"</div>
  </div>"
  html(lib*toadd)
end
route("/lib", method = POST) do
  cd(raw"..\loaded")
  if infilespayload(:yourfile)
    write(filespayload(:yourfile))
  end
  cd(raw"..\src")
  redirect("/lib")
end

route("/", method = POST) do
  Threads.lock(lock)
  if infilespayload(:yourfile)
    write(filespayload(:yourfile))
    open(filename(filespayload(:yourfile)), "r") do io
        S=read(io,String)
    end
    print("time for input")
    @time begin
    global cGraph = generate_graph(filename(filespayload(:yourfile)))
    end
    global isFileUploaded=1
    Threads.unlock(lock)
  else
    time = @time begin
    global cGraph = generate_graph("empty.json")
    end
    global isFileUploaded=1
    Threads.unlock(lock)
  end
  redirect("/app")
end


route("/app") do
    html(S)
end

route("/test") do
  html(S2)
end


route("/xml",method = POST) do
  io = open("payload.txt","w") 
  graphstr =  string(jsonpayload()["mxGraphModel"])
  graphstr2 =  string(jsonpayload())
  print(graphstr2)
  write(io,graphstr)
  j = JSON.parse(graphstr)
  global cGraph=update_graph(j,cGraph)
  graph_to_code(cGraph)
  close(io)
end


route("/upload",method = POST) do
  if infilespayload(:yourfile)
    write(filespayload(:yourfile))
    open(filename(filespayload(:yourfile)), "r") do io
        S=read(io,String)
        print(S)
    end
    global cGraph = add_to_graph(filename(filespayload(:yourfile)),cGraph)
    print("reached add to graph")
  end
  print("after add to graph")
end


route("/graph.json") do
    while isFileUploaded==0 
        Threads.lock(lock)
        Threads.unlock(lock)
        sleep(0.01)
    end
    graph_to_json(cGraph)
end

route("/export.json") do
  while isFileUploaded==0 
      Threads.lock(lock)
      Threads.unlock(lock)
      sleep(0.01)
  end
  a=""
  @time begin
  JSON.json(graph_to_file(cGraph))
  end
end


route("/edges.json") do
    while isFileUploaded==0 
        Threads.lock(lock)
        Threads.unlock(lock)
        sleep(0.01)
    end
    graph_to_edges(cGraph)
end
route("/operations.json") do
  while isFileUploaded==0 
      Threads.lock(lock)
      Threads.unlock(lock)
      sleep(0.01)
  end
  graph_to_op(cGraph)
end

route("/library.json") do
  loaded_graphs("../loaded")

end


route("/code.py") do
  while isFileUploaded==0 
      Threads.lock(lock)
      Threads.unlock(lock)
      sleep(0.01)
  end
  graph_to_code(cGraph)
end

up(8008)

while true
    readline()
end