using  JSON, Graphs, TikzGraphs, TikzPictures, LaTeXStrings
using Graphs,GraphPlot,Compose,Cairo
using SparseArrays: SparseMatrixCSC, sparse
using Base: OneTo
using LinearAlgebra: eigen
using ArnoldiMethod: SR
using Genie, Genie.Router
using Genie.Renderer, Genie.Renderer.Html, Genie.Renderer.Json, Genie.Requests



form = """
<h1>Computational Graph Editor</h1> <br>
<p>Please upload your json graph file or create new graph. </p><br>
<form action="/" method="POST" enctype="multipart/form-data">
  <input type="file" name="yourfile" /><br/>
  <input type="submit" value="Upload your graph" />
</form>
or
<form action="/" method="POST" enctype="multipart/form-data">
  <input type="submit" value="New graph" />
</form>
"""

route("/") do
  html(form)
end

route("/", method = POST) do
  if infilespayload(:yourfile)
    write(filespayload(:yourfile))
    open(filename(filespayload(:yourfile)), "r") do io
        S=read(io,String)
        print(S)
    end
  else
    "No file uploaded"
  end
end

up()

  readline()