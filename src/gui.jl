using Gtk, Graphics, Cairo

glade = GtkBuilder(filename = "Unsaved 1.glade")


window = glade["window2"]
drawboard = glade["draw"]
button = glade["b1"]

r=0

function on_button_clicked(w)
    r=1
@guarded draw(can) do widget
  ctx = getgc(can)
  h = height(can)
  w = width(can)
  drawLine(ctx,100,100,200,150,r,0,1)
end
  end

  function on_button_clicked_cancel(w)
    if(ask_dialog("Are you sure?", "Yes", "No")==0)
        exit()
    end
  end
  signal_connect(on_button_clicked, button, "clicked")

can = Gtk.Canvas(300,300)


function drawLine(c,ix,iy,ex,ey,r,g,b)
  set_line_width(c,1)
  move_to(c,ix,iy)
  line_to(c,ex,ey)
  set_source_rgb(c,r,g,b)
  stroke(c)
  fill(c)
end


push!(drawboard,can)
@guarded draw(can) do widget
  ctx = getgc(can)
  h = height(can)
  w = width(can)
  drawLine(ctx,100,100,200,150,r,0,1)
end

c = Condition()
endit(w) = notify(c)
signal_connect(endit, window, :destroy)
showall(window)
wait(c)
